## REQUIREMENTS
- Docker

## Deploy DataBase
```
$ docker run --name mongo -p 27017:27017 -d mongo --smallfiles
```
## DEPLOY CONNECTOR

    $ cd connector/
    $ docker build -t connect_kc .
    $ docker run -idt --rm --name connect_kc --link mongo:mongo --link kafka:kafka connect_kc


