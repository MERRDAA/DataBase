from kafka import KafkaConsumer
from pymongo import MongoClient
import sys
import time
import json
import traceback
import datetime

consumer = KafkaConsumer('401_windows', group_id='my-group', bootstrap_servers=['kafka:9092'])
client = MongoClient('mongo', 27017)
db = client.test_database
sound_records = db.sound_records

for message in consumer:
	try:
		js = json.loads(message.value)
		ts = datetime.datetime.utcfromtimestamp(int(time.time())).isoformat().replace("T", " ")
		for key, value in js.items:
			# key = nome sensor
			# value = valor som
			room_id = 1 # temporário
			x = sound_records.update_one({"time_stamp":str(ts)}, {'$set': {"sensor_id": str(sensor), "sound_value": str(value)}}, upsert=True)
			print(x)
	except Exception as e:
		traceback.print_exc()
		sys.exit(1)


"""
Para já

Salas: 
    id UUID PRIMARY KEY, 
    room_name text,
    description text 

Sensor:
	id
	sala
	nome

Sound records
	sensor id
	time stamp
	sound value
"""